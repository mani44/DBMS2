var express = require("express");
var router = express.Router();
var Car = require("../models/car");
/* GET home page. */
router.get("/", (req, res, next) => {
  Car.find({}, (err, data) => {
    res.render("index", { title: "DBMS Project", data: data });
  });
});

router.get("/id", (req, res, next) => {
  res.render("payment", { title: "DBMS Project" });
});

router.get("/:brand", (req, res, next) => {
  var idd = req.path;
  idd = idd.substr(1);
  Car.find({ brand: idd }, (err, data) => {
    console.log(idd);
    console.log(data);
    res.render("name", { title: "DBMS Project", data: data });
  });
});

module.exports = router;
